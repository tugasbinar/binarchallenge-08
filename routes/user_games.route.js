const express = require('express')
const router = express.Router()
const usergamesController = require('../controller/user_gamesController')

router.post('/send-forgot-pass-token', usergamesController.sendForgotPasswordToken)
router.post('/verify-forgot-pass-token', usergamesController.verifyForgotPasswordToken)
router.post('/change-pass', usergamesController.changePassword)

module.exports = router