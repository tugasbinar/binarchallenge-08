const express = require('express')
const router = express.Router()
const user_gamesroute = require('./user_games.route')
const AuthController = require('../controller/auth.controller')
const { body, validationResult } = require('express-validator');

const jwt = require ('jsonwebtoken')

const multer = require('multer')
const storage = require('../services/multerStorage.service');
const upload = multer({
    storage,
    limits: {
      fileSize: 3000000000000000000
    },
    fileFilter: (req, file, cb) => {
      if (file.mimetype === 'video/mp4') {
        cb(null, true)
      } else {
        cb({
          status: 400,
          message: 'File type does not match'
        }, false)
      }
    }
  })

  router.post('/upload',
  upload.single('video'),
  [
  body('username').notEmpty(),
  body('password').notEmpty(),
],
  (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: "Data cannot be empty" 
      }
    } else {
      next()
    }
  },AuthController.create
)

router.get('/usergames',
(req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
    }
  }
  }, AuthController.list)

  router.get('/usergames/:id',
  (req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
      
    }
  }
  },
  AuthController.getById)

  router.put('/usergames/:id',
  (req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
    }
  }
},
  [
  body('password')
  .optional()
  .notEmpty()
  ],
  (req, res, next) => {
  const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: "Usename or Password failed"
      }
    } else {
      next()
    }
  },
  AuthController.update)

  router.delete('/usergames/:id', (req, res, next) => {
    if (req.headers.authorization) {
      const user = jwt.decode(req.headers.authorization)
      req.user = user
      next()
    } else {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    }
  }, AuthController.delete)

router.post('/register', AuthController.register)
router.post('/loginpassword', AuthController.loginPassword)
router.post('/login-google', AuthController.loginGoogle)
router.use('/usergame', user_gamesroute)

module.exports = router