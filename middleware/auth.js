const jwt = require("jsonwebtoken");

const auth = async (req, res, next) => {
  console.log('ok')
  try {
    if (!req.headers.authorization) {
      console.log('test')
      throw {
        status: 401,
        message: "Unauthorized request",
      };
    } else {
      const user = jwt.decode(req.headers.authorization);
      if (user) {
        console.log(user)
        req.user = user;
        next();
      } else {
        console.log('test2')
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }
    }
  } catch (err) {
    next(err);
  }
};

module.exports = auth;
